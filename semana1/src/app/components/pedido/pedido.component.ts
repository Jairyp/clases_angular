import { Component, OnInit,Input, HostBinding } from '@angular/core';
import { Producto } from 'src/app/models/productos.model';

@Component({
  selector: 'app-pedido',
  templateUrl: './pedido.component.html',
  styleUrls: ['./pedido.component.css']
})
export class PedidoComponent implements OnInit {

  @Input() pedido:Producto;
  @HostBinding('attr.class') cssClass = 'col-md-4';
  constructor() {
    
    
   }

  ngOnInit() {
  }

}
