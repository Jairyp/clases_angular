import { Component, OnInit } from '@angular/core';
import { Producto } from 'src/app/models/productos.model';

@Component({
  selector: 'app-detallepedidos',
  templateUrl: './detallepedidos.component.html',
  styleUrls: ['./detallepedidos.component.css']
})
export class DetallepedidosComponent implements OnInit {
  pedidos:Producto[];
  constructor() {
    this.pedidos = [];
   }

  ngOnInit() {
  }

  guardar(n:string,url:string):boolean{
    this.pedidos.push(new Producto (n,url))
    return false;
  }

}
