import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetallepedidosComponent } from './detallepedidos.component';

describe('DetallepedidosComponent', () => {
  let component: DetallepedidosComponent;
  let fixture: ComponentFixture<DetallepedidosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetallepedidosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetallepedidosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
